import React from 'react';
import { useLocation } from 'react-router-dom';
import { Navbar, Nav, Container } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

// function HeaderView() {
//   let location = useLocation();
//   console.log(location.pathname);
//   return true;  
// }

const Header = () => {
  return (
    <header>
      <Navbar bg='dark' variant='dark' expand='lg' collapseOnSelect>
        <Container>
          <LinkContainer to='/'>
            <Navbar.Brand >CTS Bank</Navbar.Brand>
          </LinkContainer>
          <Navbar.Toggle aria-controls='basic-navbar-nav' />
          <Navbar.Collapse id='basic-navbar-nav'>
            <Nav className="mr-auto">
              <Nav.Link href="#DashBoard">Home</Nav.Link>
              <Nav.Link href="/Registration">Register</Nav.Link>
              <Nav.Link href="#ApplyLoan">Apply Loan</Nav.Link>
              <Nav.Link href="#Update_Account">Update Account</Nav.Link>
            </Nav>
            <Nav className='ml-auto'>
              <LinkContainer to='/'>
                <Nav.Link>
                  <i className='fas fa-user'></i> Sign in
                </Nav.Link>
              </LinkContainer>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </header>
  );
};

export default Header;
