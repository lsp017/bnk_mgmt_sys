import React from "react";
import countries from "./countries.js";
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';

export default function App() {
  const [email, setEmail] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [country, setCountry] = React.useState("");
  const [acceptedTerms, setAcceptedTerms] = React.useState(false);
  const [date, setDate] = React.useState("");
  const [age, setAge] = React.useState("");
  // customerName
  // customerId
  // GuardianType
  // GuardianName
  // Address
  // Citizenship
  // State
  // Country
  // gender
  // maritalStatus
  // contactNo
  // dob
  // registrationDate
  // accountType
  // branchName
  // citizenStatus
  // initialDepositAmt
  // pan
  // age


  const handleSubmit = (event) => {
    console.log(`
      Email: ${email}
      Password: ${password}
      Country: ${country}
      Accepted Terms: ${acceptedTerms}
      date : ${date}
      age: ${age}
    `);

    event.preventDefault();
  }

  const getAge = (e) => {
    setDate(e.target.value);
    var today = new Date();
    var birthDate = new Date(date);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    setAge(age);
    console.log(e.target.value);

  }
  return (
    <Form onSubmit={handleSubmit}>
      <Form.Row>
        <Form.Group as={Col} controlId="formGridFName">
          <Form.Label>First Name</Form.Label>
          <Form.Control type="text" placeholder="First Name" value={email} onChange={e => setEmail(e.target.value)} />
        </Form.Group>

        <Form.Group as={Col} controlId="formGridMName">
          <Form.Label>Middle Name</Form.Label>
          <Form.Control type="text" placeholder="Middle Name" value={password} onChange={e => setPassword(e.target.value)} />
        </Form.Group>
        <Form.Group as={Col} controlId="formGridLName">
          <Form.Label>Last Name</Form.Label>
          <Form.Control type="text" placeholder="Last Name" value={password} onChange={e => setPassword(e.target.value)} />
        </Form.Group>
      </Form.Row>

      <Form.Row>
        <Form.Group as={Col} controlId="formGridEmail">
          <Form.Label>Email</Form.Label>
          <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} />
        </Form.Group>

        <Form.Group as={Col} controlId="formGridPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)} />
        </Form.Group>
      </Form.Row>
      <Form.Row>
        <Form.Group as={Col} controlId="formGrid">
          <Form.Label>DOB</Form.Label>
          <Form.Control type="date" value={date} onChange={e => getAge(e)} />
        </Form.Group>
        <Form.Group as={Col} controlId="formGridAge">
          <Form.Label>Age</Form.Label>
          <Form.Control type="text" value={age} readOnly />
        </Form.Group>
      </Form.Row>
      <Form.Row>
        <Form.Group as={Col} controlId="formGridAddress1">
          <Form.Label>Address Line 1</Form.Label>
          <Form.Control placeholder="1234 Main St" />
        </Form.Group>
        <Form.Group as={Col} controlId="formGridAddress2">
          <Form.Label>Address Line 2</Form.Label>
          <Form.Control placeholder="1234 Main St" />
        </Form.Group>
      </Form.Row>

      <Form.Row>
        <Form.Group as={Col} controlId="formGridCity">
          <Form.Label>City</Form.Label>
          <Form.Control />
        </Form.Group>

        <Form.Group as={Col} controlId="formGridState">
          <Form.Label>State</Form.Label>
          <Form.Control />
        </Form.Group>

        <Form.Group as={Col} controlId="formGridCountry">
          <Form.Label>Country</Form.Label>
          <Form.Control as="select" value={country} onChange={e => setCountry(e.target.value)}>
            <option key=""></option>
            {countries.map(country => (
              <option key={country}>{country}</option>
            ))}
          </Form.Control>
        </Form.Group>

        <Form.Group as={Col} controlId="formGridZip">
          <Form.Label>Zip</Form.Label>
          <Form.Control />
        </Form.Group>
      </Form.Row>
      <Form.Row>
        <Form.Group as={Col} controlId="formGridCitizenship">
          <Form.Label>Citizenship</Form.Label>
          <Form.Control type="text" placeholder="Citizenship" value={email} onChange={e => setEmail(e.target.value)} />
        </Form.Group>

        <Form.Group as={Col} controlId="formGridGender">
          <Form.Label>Gender</Form.Label>
          <Form.Control as="select" defaultValue=" " placeholder="Gender" value={password} onChange={e => setPassword(e.target.value)} >
          <option> </option>
            <option>Male</option>
            <option>Female</option>
          </Form.Control>
        </Form.Group>
        <Form.Group as={Col} controlId="formGridMaritalStatus">
          <Form.Label>Marital Status</Form.Label>
          <Form.Control as="select" defaultValue=" " placeholder="Marital Status" value={password} onChange={e => setPassword(e.target.value)} >
          <option> </option>
            <option>Married</option>
            <option>Unmarried</option>
          </Form.Control>
        </Form.Group>
      </Form.Row>

      <Form.Group id="formGridCheckbox">
        <Form.Check type="checkbox" label="Check me out" onChange={e => setAcceptedTerms(e.target.value)} />
      </Form.Group>

      <Button variant="primary" type="submit">
        Submit
  </Button>
    </Form>
  );
}
