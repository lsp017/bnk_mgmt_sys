import React from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter, Route } from 'react-router-dom';

import Header from './components/Header';
import Footer from './components/Footer';
import Login from './screens/Login';
import Registration from './screens/Registration';



const App = () => {
  return (
    <BrowserRouter>
      <Header  />
      <main className='py-3'>
        <Container>
        <Route path='/' exact component={Login} />
          <Route path='/Registration' exact component={Registration} />
        </Container>
      </main>
      <Footer />
    </BrowserRouter>
  );
};

export default App;


